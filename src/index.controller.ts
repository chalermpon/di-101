import {
    Controller,
    Get,
    Inject,
} from '@nestjs/common'
import { LibraryRepository } from './repository/library'

@Controller('/')
export class IndexController {
    constructor(
        @Inject(LibraryRepository)
        private readonly _libraryRepository: LibraryRepository,
    ) {
        console.log(this._libraryRepository)
    }

    @Get('/')
    public testController() {
        return this._libraryRepository.getByISBN('something')
    }
}
