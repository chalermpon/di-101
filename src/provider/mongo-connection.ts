import { Provider } from '@nestjs/common'
import { MongoClient } from 'mongodb'

export const mongoConnectionProvider: Provider = {
    provide: 'MONGO_CONNECTION', // named this provider
    inject: ['ENV_CONFIG'], // dependency injection
    useFactory: async (config) => {
        const dbConfig = config.database
        const mongoClient = new MongoClient(
            `mongodb://${dbConfig.host}:${dbConfig.port}`,
            {useNewUrlParser: true},
        )
        await mongoClient.connect()
        return await mongoClient.db(dbConfig.dbName) // return mongo's Db instance
    },
}
