export interface ILibraryRepository {
    getByISBN(isbn: string)
}
